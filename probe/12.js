'use strict';

// Методы перебора массивов
// filter

// const names = ['Ivan', 'Oleg', 'Kirill', 'Voland']
// const shortNames = names.filter(name => name.length < 5)
// console.log(shortNames)

//map

// let answers = ['IvAn', 'AnnA', 'Hello'];
// const result = answers.map(item => item.toLowerCase())
// console.log(result)

//every и some

// const some = [4, 'qwq', 'sdf'];
// console.log(some.some(item => typeof(item) === 'number')); //хотя бы 1 элемент число
// console.log(some.every(item => typeof(item) === 'number')) // все элементы числа

//reduce

// const arr = [4, 5, 1, 3, 2, 6];

// const res = arr.reduce((sum, current) => sum + current);
// console.log(res)

// const arr = ['apple', 'pear', 'plum'];

// const res = arr.reduce((sum, current) => `${sum}, ${current}`);
// console.log(res)

// const arr = [4, 5, 1, 3, 2, 6];

// const res = arr.reduce((sum, current) => sum + current, 10); //после функции можем указать начальное значение, например здесь это 10
// console.log(res)

const obj = {
    ivan: 'persone',
    ann: 'persone',
    dog: 'animal',
    cat: 'animal'
};

const newArr = Object.entries(obj)
.filter(item => item[1] == 'persone')
.map(item => item[0])
console.log(newArr)